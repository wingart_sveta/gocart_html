if($(".flexslider").length){
  include("js/jquery.flexslider.js");
}

if($(".tabs").length){
  include("js/easyResponsiveTabs.js");
}

if($(".popup_open").length){
      include("js/jquery.arcticmodal.js"); 
    }

if($(".rating_stars").length){
   include("js/jquery.raty.js");
 }

 if($(".phone").length){
   include("js/maskedInput.js");
 }

if($(".owl-carousel").length){
   include("js/owl.carousel.js");
 }

if($(".styler").length){
    include("js/jquery.formstyler.js");
}

if($(".range").length){
    include("js/ion.rangeSlider.min.js");
}

if($(".scroll_pane").length){
      include("js/jquery.jscrollpane.js");
}

if($(".data-mh").length){
      include("js/jquery.matchHeight-min.js");
}
 //----Include-Function----
function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}





$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

//-------form validation-------------------

  function checkEmail(currInput){
      var pattern=/^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,4}$/;

      if(!pattern.exec(currInput.val())){
          return false;
        }
        else{
          return true;
        }
    }

  function checkPass(currInput){
    var pattern=/^(?=.*\d)(?=.*[a-z])(?!.*\s).*$/;

    if(!pattern.exec(currInput.val())){
      return false;
    }
    else{
      return true;
    }
  }

  function checkName(currInput){
      var pattern=/^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/;
      if(!pattern.exec($(currInput).val())){
        return false;
      }
      else{
        return true;
      }
    }

  function checkPhone (currInput) {
      var pattern = /(\(?\d{3}\)?[\- ]?)?[\d\- ]{4,10}$/;


      if(!pattern.exec(currInput.val())){
        return false;
      }
      else{
        return true;
      }
    }

    $('.sendBtn').click(function(e){

    
    var errors = false;

      var currentForm = $(this).closest("form.formSend"),
          mainPhone   = currentForm.find('input[name="mainPhone"]'),
          firstName   = currentForm.find('input[name="firstName"]');
          lastName    = currentForm.find('input[name="lastName"]');
          city        = currentForm.find('input[name="city"]'),
          email       = currentForm.find('input[name="email"]'),
          password    = currentForm.find('input[name="password"]'),
          mainAddress = currentForm.find('input[name="mainAddress"]'),
          postcode    = currentForm.find('input[name="postcode"]'),
          company     = currentForm.find('input[name="company"]'),
          state       = currentForm.find('select[name="state"] option:selected'),
          birthDate   = currentForm.find('select[name="birthDate"] option:selected'),
          birthMonth   = currentForm.find('select[name="birthMonth"] option:selected'),
          birthYear   = currentForm.find('select[name="birthYear"] option:selected');
         


      if(email.length){
        if(!email.val().length || !checkEmail(email)){
          console.log("mail invalid");
          email.parent().addClass("invalid");
            errors = true;
            console.log(2);
        }
        else{

          email.parent().removeClass("invalid");
        }
      }

      if(password.length){
          if(!password.val().length || !checkPass(password)){

            password.parent().addClass("invalid");
              errors = true;
              console.log(2);
          }
          else{

            password.parent().removeClass("invalid");
        }
      }

      if(mainPhone.length){
          if(!mainPhone.val().length || !checkPhone(mainPhone)){

            mainPhone.parent().addClass("invalid");
              errors = true;
              console.log(2);
          }
          else{

            mainPhone.parent().removeClass("invalid");
        }
      }

      if(firstName.length){
          if(!firstName.val().length || firstName.val() =="First name"){

            firstName.css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            firstName.css({"border":"1px solid #e9e9e9"});
          }
      }

      if(lastName.length){
          if(!lastName.val().length || lastName.val() =="Last name"){

            lastName.css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            lastName.css({"border":"1px solid #e9e9e9"});
          }
      }

      if(postcode.length){
          if(!postcode.val().length || postcode.val() =="Zip/Postcode" || !checkPhone(postcode)){

            postcode.parent().addClass("invalid");
              errors = true;
              console.log(2);
          }
          else{

            postcode.parent().removeClass("invalid");
        }
      }

      if(city.length){
          if(!city.val().length || city.val() =="City"){

              city.parent().addClass("invalid");
              errors = true;
              console.log(2);
         
          }
          else{

             city.parent().removeClass("invalid");
          }
      }

      if(mainAddress.length){
          if(!mainAddress.val().length || mainAddress.val() =="Address line"){

              mainAddress.parent().addClass("invalid");
              errors = true;
              console.log(2);
         
          }
          else{

             mainAddress.parent().removeClass("invalid");
          }
      }

      if(state.length){
         
          if(state.val() == "State"){

            state.parent().next().css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            state.parent().next().css({"border":"1px solid #e9e9e9"});
          }
      }

      if(birthMonth.length){
         
          if(birthMonth.val() == "Month"){

            birthMonth.parent().next().css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            birthMonth.parent().next().css({"border":"1px solid #e9e9e9"});
          }
      }

      if(birthDate.length){
         
          if(birthDate.val() == "Day"){

            birthDate.parent().next().css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            birthDate.parent().next().css({"border":"1px solid #e9e9e9"});
          }
      }

      if(birthYear.length){
         
          if(birthYear.val() == "Years"){

            birthYear.parent().next().css({"border":"1px solid red"});
            errors = true;
            console.log(2);
          }
          else{

            birthYear.parent().next().css({"border":"1px solid #e9e9e9"});
          }
      }




      if(errors){
        e.preventDefault();
      }
  
  });




$(document).ready(function(){

    /*-----Выпадашка------*/

      if($('.filter_li_text').length){
        $('.filter_li_text').on('click', function(){
          $(this)
            .toggleClass('active')
            .next('.filter_list2')
            .slideToggle()
            .parents(".filter_list_item")
            .siblings(".filter_list_item")
            .find(".filter_li_text")
            .removeClass("active")
            .next(".filter_list2")
            .slideUp();
        });

        $('.filter_li_inner').on('click', function(){
          $(this)
            .toggleClass('active')
            .next('.filter_list3')
            .slideToggle()
            .parents(".filter_list2_item")
            .siblings(".filter_list2_item")
            .find(".filter_li_inner")
            .removeClass("active")
            .next(".filter_list3")
            .slideUp();
        });    
      }

    //  spoiler page catalog------------
    $(".button_style1").on("click", function(){
          $(this).next().toggleClass("active");
      });

    // Slider page product

    if($(".flexslider").length){
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
      });
    }
    //---- owl-carousel
      if($(".owl-carousel").length){
        $(".owl-carousel").each(function(){
          var $this = $(this);

            if($this.hasClass('carousel_one')){

                if($this.hasClass('main_carousel')){

                    $this.owlCarousel({
                        singleItem:true,
                        addClassActive:true,
                        transitionStyle : "fade"
                    });
                  
                  }
                  else{
                    $this.owlCarousel({
                        singleItem:true,
                        addClassActive:true
                    });

                }

              }
              else if($this.hasClass('carousel_brend')){
              
                $this.owlCarousel({
                  items:5,
                  itemsDesktop:[1120,5],
                  itemsDesktopSmall:[979,4],
                  itemsTablet: [768,3],
                  itemsTabletSmall: [600,2],
                  itemsMobile:[479,2]
                });

              }
              else{
                $this.owlCarousel({
                  items:4,
                  itemsDesktop:[1120,3],
                  itemsDesktopSmall:[979,3],
                  itemsTablet: [768,2],
                  itemsTabletSmall: [600,2],
                  itemsMobile:[479,1]
                  
                });
              }
        });
      }

    //-----responsive tabs----

      if($(".tabs").length){
        $(".tabs").easyResponsiveTabs(); 
      };
    //------------sticky menu---------
    if($(".sticky").length){
          
      var stickyHeader = {

         init: function(){

            this.sticky = $('.sticky');
            this.w = $(window);
            this.body = $('body');

            this.initHeaderParameters();
            this.toggleSticky();
            this.bindEvents();

         },

         initHeaderParameters: function(){

            var self = this;

            self.disableSticky();

            if(self.w.width() < 768){

             self.w.off('scroll.sticky');
             return false;

            }
            else{

             self.bindEvents();

            }

            self.hHeight = self.sticky.outerHeight();
            self.hOffset = self.sticky.offset().top;
            self.sticky.data('stickyInit', true);

            self.toggleSticky();

         },

         toggleSticky: function(){

            var self = this;

            if(self.w.scrollTop() > self.hOffset && !self.sticky.hasClass('stickyScroll')){
             self.sticky.addClass('stickyScroll');
             self.disableEmptyArea(true);
            }
            else if(self.w.scrollTop() <= self.hOffset){
             self.sticky.removeClass('stickyScroll');
             self.disableEmptyArea(false);
            }

         },

         disableSticky: function(){

            var self = this;

            self.sticky.removeClass('stickyScroll');
            self.disableEmptyArea(false);

         },

         bindEvents: function(){

            var self = this;

            self.w.on('scroll.sticky', function(){

             self.toggleSticky();

          });

          self.w.on('resize.sticky', self.initHeaderParameters.bind(self));

         },

         disableEmptyArea: function(isNeed){

            var self = this;

            if(isNeed){
             self.body.css('padding-top', self.hHeight);
            }
            else{
             self.body.css('padding-top', 0);
            }

         }

        }
          stickyHeader.init();


    };

    //----rating stars-----
   
      if ($('.rating_stars').length){
        $('.rating_stars').each(function(){
          var itemStar = $(this).attr("data-star");
          if(!$(this).hasClass('staticStar')){
            $(this).raty({
              score: function() {
                return $(this).attr('data-star');
              }
            });
          }
          else{
              $(this).raty({ readOnly: true, score: itemStar });    
          }
        });
      }

    // ------popup------------------------

      if($(".popup_open").length){
          $(".popup_open").on('click',function(){
              var modal = $(this).data("modal");
              $(modal).arcticmodal( );
              $('.popup_open' + $(this).data('order')).arcticmodal();
          });
      };
    
    
    //-- formstyler start
      if($(".styler").length){
          $(".styler").styler();  
      }

   //---maskedinput---------------- 
  $(function($){
    if( $(".phone").length){
       $(".phone").mask("(9 9 9) 9 9 9 9-9 9 9", {placeholder:"_"});
    }
  });

  
 //-------ion.rangeSlider----
    if ($('.range').length){

        $("#amount2").change(function(e){
          var inputValue = $(this).attr("value");
          if(inputValue){
              $(".range").ionRangeSlider("update",{
              "to": inputValue
              })
          }
        })
        $(window).resize(function(){
            $(".range").ionRangeSlider("update");
        });

        $(".range").ionRangeSlider({
          min:0,
          max:6000,
          type:'double',
          onChange: function (obj) {      // callback, вызывается при каждом изменении состояния
              $("#amount1").val(obj["from"]);
              $("#amount2").val(obj["to"]);
              }
          });

          var slider = $(".range").data("ionRangeSlider");
            $("#amount1").on("change", function(){
              var newVal = $(this).val();
              slider.update({
                  from: newVal
              });
            })

          $("#amount2").on("change", function(){
            var newVal = $(this).val();
            slider.update({
                to: newVal
            });
          });

          $('#amount1, #amount2').keypress(function(event){
            var key, keyChar;
            if(!event) var event = window.event;
              if (event.keyCode) key = event.keyCode;
              else if(event.which) key = event.which;
              if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
              keyChar=String.fromCharCode(key);
              if(!/\d/.test(keyChar)) return false;
            });

          
          }

    //-------------show_menu-----
     $(".show_nav_btn, .menu_closed").on("click", function(){
        $("body").toggleClass("show_menu");
     
      })


    //-- for product_not_box hasClass notavailable

      if($(".product_not_box").length){
          $(".product_not_box").each(function(){

            if($(this).hasClass('notavailable')){
              $(this).find('.add_type_lk').text('not available');
            }

          });
      };

    //----------------------
       $(".video_big_abs").on("click", function(){
          $(this).addClass("active");
          var videoUrl = $(this).parent().find("iframe").attr("src");

          $(this).parent().find("iframe").attr("src", videoUrl + '?autoplay=1');

        });

  
       $(".filter_sort_lk").on("click", function(){
          $(this).toggleClass("active");
       });

    //---clouse bl free shipping

      $(".headertop_resp_close").on("click", function(){

          $(this).parents('header').addClass('active');
          $(this).parents('header').next('#content').addClass('active');
          $(this).parents('.headertop_resp').remove();
      }); 

    //----click search-----
    if($(".header_search_btn").length){

        $(".header_search_btn").on("click", function(){
            $(this).next().slideToggle().toggleClass("active");
            $(this).toggleClass("active");


        });

      }
    //---clouse mobail search----
     $(".search_resp_close").on("click", function(){
          $(this).parent().slideUp().removeClass("active");
      });

     //---open mobail categories----
    
       if ($(window).width()<770){

            $(".categories_item_lk").on("click", function(e){

                if(!$(this).next().hasClass("active")){

                    $(this).next().addClass("active");
                    $(this).parent().siblings().find(".menu_item_show").removeClass("active");

                    e.preventDefault();
                }

            });
          }

    //---open details table account ----
     $(".details_lk").on("click", function(){
        $(this).toggleClass("active");
        $(this).parents('tr').next().find('.account_views').slideToggle().toggleClass("active");

      });

    
     //---active submenu_item ----
     $(".categories_show").on("mouseenter", function(){
        $(this).parent().find('.submenu_lk').addClass("active");
       
      });

    $(".categories_show").on("mouseleave", function(){
   
     $(this).parent().find('.submenu_lk').removeClass("active");

    });

    //---basket_show--------- ----
    $(".basket_hover").on("mouseenter", function(){
        $(this).find('.basket_show_inner').addClass("active");
        if($('.basket_show_inner').is(":visible")){
             $('.scroll_pane').jScrollPane();
          }
       
      });

    $(".basket_hover").on("mouseleave", function(){
   
     $(this).find('.basket_show_inner').removeClass("active");

    });

    //-------------show_ask question----
      $(".product_question_btn, .address_add_lk, .add_address_btn, .edit_btn").on("click", function(){
        $("body").addClass("show_block");
      });

      $(".question_close").on("click", function() {
        $("body").removeClass("show_block");
      });

    //-------------show_filter----
    if($(".view_filter_lk").length){

      $(".view_filter_lk").on("click", function(){
        $("body").addClass("show_block");
      });

    }
    //------------addcompany name------
    $(".add_company_btn").on("click", function(){
        $(this).next().addClass('active');
      });
    //-------------.close_engine--------
     $(".close_engine").on("click", function(){
        $(this).parent().remove();
      });

    //-------------.close_tr cart_tb--------
     $(".last_col").on("click", function(){
        $(this).parents('tr').remove();
      });

    //-------------.close wish item--------
     $(".wish_clear_lk").on("click", function(){
        $(this).parents('.product_item').remove();
      });

})


  $(document).on("click ontouchstart", function(event){
    
    if($(event.target).closest('.product_question_btn, .product_question_show, .address_add_lk, .add_address_show, .add_address_btn, .edit_btn, .view_filter_lk, .filter_show, .close_engine').length)return;
      $("body").removeClass("show_block");
    
      
    event.stopPropagation()
  })

 




  $(document).on("click ontouchstart", function(event){
    
    if($(event.target).closest('.search_show').length || $(event.target).closest('.header_search_btn').length) return;
    
      $('.search_show').removeClass("active");
      $(".search_show").slideUp();
      $('.header_search_btn').removeClass("active");
      $('.search_resp_close ').removeClass("active");
      
    event.stopPropagation()
  })
